# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Schedule(models.Model):
    message_title = models.CharField(max_length=255)
    message_body = models.CharField(max_length=255)
    conditional_clause = models.CharField(max_length=255)
    time = models.CharField(max_length=255)
