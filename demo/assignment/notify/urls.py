from django.conf.urls import url

from .views import schedule_message

urlpatterns = [
    url(r'^schedulemessage/', schedule_message),
]