# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.core import serializers
from rest_framework.decorators import api_view
from rest_framework.response import Response
from push_notifications.models import APNSDevice, GCMDevice
from django.shortcuts import render, render_to_response

# Create your views here.
from .models import Schedule
import threading
import datetime


@api_view(['POST'])
def schedule_message(request):
    print "request.data"
    save_msg = Schedule(**request.data).save()
    return Response("success")


def send_message():
    secondlist = []
    now = datetime.datetime.now().replace(minute=0, second=0, microsecond=0)
    print "current time", now
    morning_time = now.replace(hour=06, minute=0, second=0, microsecond=0)
    afternoon_time = now.replace(hour=12, minute=0, second=0, microsecond=0)
    evening_time = now.replace(hour=18, minute=0, second=0, microsecond=0)
    print "compare time", morning_time
    if now == morning_time:
        print "morning"
        get_schedule = Schedule.objects.filter(time='morning')
        serial = serializers.serialize('json', get_schedule)
        final = json.loads(serial)
        for i in final:
            secondlist.append(i['fields'])
            data = {'result': secondlist}
            print "serilized", data
            devices = GCMDevice.objects.all()
            devices.send_message(data)
    elif now == afternoon_time:
        print "after noon"
        get_schedule = Schedule.objects.filter(time='noon')
        serial = serializers.serialize('json', get_schedule)
        final = json.loads(serial)
        for i in final:
            secondlist.append(i['fields'])
            data = {'result': secondlist}
            print "serilized", data
            devices = GCMDevice.objects.all()
            devices.send_message(data)
    else:
        print "evening"
        get_schedule = Schedule.objects.filter(time='evening')
        serial = serializers.serialize('json', get_schedule)
        final = json.loads(serial)
        for i in final:
            secondlist.append(i['fields'])
            data = {'result': secondlist}
            print "serilized", data
            devices = GCMDevice.objects.all()
            devices.send_message(data)
    threading.Timer(3600, send_message).start()


send_message()
